package time;
import static org.junit.Assert.*;

import org.junit.Test;
/**
 * 
 * @author Bhaumik patel
 * Student number: 991536277
 */
public class TimeTest {

	@Test
	public void testGetTotalMillisecondsRegular( ) 
	{
		int totalMilliseconds = Time.getTotalMillisecond("12:05:50:05");
		assertTrue("Invalid number of millisecond", totalMilliseconds == 5);
	}
	
	@Test(expected=NumberFormatException.class)
	public void testGetTotalMilliSecondsException()
	{
		int totalMilliseconds = Time.getTotalMillisecond("01:01:00:0a" );
		fail("The time provided is not valid");
	}
	
	@Test 
	public void testGetTotalMilliSecondsBoundryIn()
	{
		int totalMilliseconds = Time.getTotalMillisecond("12:05:05:999" );
		assertTrue("The time provided does not match the result", totalMilliseconds == 999);
	}
	
	@Test (expected=NumberFormatException.class)
	public void testGetTotalMillisecondsBoundaryOut() {
		int totalMilliseconds=Time.getTotalMillisecond("12:05:50:1000");
		fail("The time provided is not valid");
	}
	
	@Test
	public void testGetTotalSecondsRegular() {
		int totalSeconds = Time.getTotalSeconds("01:01:01");
		assertTrue("The time provided does not match the result", totalSeconds == 3661);
	}	

	@Test(expected=NumberFormatException.class)
	public void testGetTotalSecondsException()
	{
		int totalSeconds = Time.getTotalSeconds("01:01:0a" );
		fail("The time provided is not valid");
	}
	
	@Test 
	public void testGetTotalSecondsBoundryIn()
	{
		int totalSeconds = Time.getTotalSeconds("00:00:00" );
		assertTrue("The time provided does not match the result", totalSeconds == 0);
	}
	
	@Test(expected=NumberFormatException.class)
	public void testGetTotalSecondsBoundryOut()
	{
		int totalSeconds = Time.getTotalSeconds("01:01:60" );
		fail("The time provided is not valid");
	}
}
